import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
import Brain from './components/Brain';
import Header from './components/common/Header';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
    <div>
        <Header />
            
        <Brain />
    </div>, 
    document.getElementById('root')
);
registerServiceWorker();
