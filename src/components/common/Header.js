import React from 'react';
import './Header.css';

class Header extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div className="Header">
                <h1>Gender Guesser</h1>
            </div>
        )
    }
}

export default Header;