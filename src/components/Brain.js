import React from 'react';
import * as brain from 'brain.js';
import * as papa from 'papaparse';
import './Brain.css';

class Brain extends React.Component {
    
    constructor() {
        super();

        this.state = {
            trained: false,
            net: [],
            output: null,
        };

        this.handleResult = this.handleResult.bind(this);
    }

    componentDidMount() {
        this.setState({
            trained: false,
            net: [],
        });

        var path = require('./gender.csv');
        papa.parse(path, {
            header: true,
            download: true,
            complete: this.handleResult
        });
    }

    

    handleResult(result) {
        console.log(result);

        const shuffle = (a) => {
            for (let i = a.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [a[i], a[j]] = [a[j], a[i]];
            }
            return a;
        }

        var favorite = {
            color: {},
            music: {},
            beverage: {},
            softdrink: {},
            gender: {},
        };

        result.data.forEach((element) => {
            Object.keys(element).forEach((key) => {
                switch (key) {
                    case "Favorite Color":
                        if (favorite.color[element[key]] == null ){
                            favorite.color[element[key]] = Object.keys(favorite.color).length;
                        }
                        break;
                    case "Favorite Music Genre":
                        if (favorite.music[element[key]] == null ){
                            favorite.music[element[key]] = Object.keys(favorite.music).length;
                        }
                        break;
                    case "Favorite Beverage":
                        if (favorite.beverage[element[key]] == null ){
                            favorite.beverage[element[key]] = Object.keys(favorite.beverage).length;
                        }
                        break;
                    case "Favorite Soft Drink":
                        if (favorite.softdrink[element[key]] == null ){
                            favorite.softdrink[element[key]] = Object.keys(favorite.softdrink).length;
                        }
                        break;
                    case "Gender":
                        if (favorite.gender[element[key]] == null ){
                            favorite.gender[element[key]] = Object.keys(favorite.gender).length;
                        }
                        break;
                    default:
                        console.log("Key matches no cases.");
                        break;
                }
            })
        });
        Object.freeze(favorite);
        console.log(favorite);
    
        //fill array with brain.js compatible array.
        var trainingData = []
        result.data.forEach((element, index) => {
            var inputElement = {
                input: {
                    color: favorite.color[element["Favorite Color"]],
                    music: favorite.music[element["Favorite Music Genre"]],
                    beverage: favorite.beverage[element["Favorite Beverage"]],
                    softdrink: favorite.softdrink[element["Favorite Soft Drink"]],
                },
                output: {
                    gender: favorite.gender[element["Gender"]],
                }
            };
            trainingData.push(inputElement);
        });

        shuffle(trainingData);
        console.log(trainingData);

        //use 50 for training. Reminder for testing.
        var trainingSubset = trainingData.slice(0, 50);
        var testingSubset = trainingData.slice(50);
        
        //TODO: Test out best network settings.
        var neuralNet = new brain.NeuralNetwork({
            hiddenLayers: [15],
        });

        //TODO: Test best training settings.
        console.log(neuralNet.train(trainingSubset), {
            log: true,
            learningRate: 0.5,
            momentum: 0.1,
        });

        //Basic test for correctness
        var correct = 0;
        testingSubset.forEach((element) => {
            console.log("Input + Output: ", element);
            var output = neuralNet.run(element.input);
            console.log(output);
            console.log("=================");

            if (element.output.gender === 1 && output.gender > .5) {
                correct++;
            }
            if (element.output.gender === 0 && output.gender < .5) {
                correct++;
            }
        });
        console.log("Amount score: ", correct, "/", testingSubset.length);
        console.log("Percentage score: " + correct/testingSubset.length);

        //TODO: Present results better.
        //trying my own input:
        var myoutput = neuralNet.run({ 
            color: 1,
            beverage: 3,
            music: 6,
            softdrink: 1,
        });
        console.log("My output");
        console.log(myoutput.gender);

        this.setState({
            net: neuralNet,
            trained: true,
            output: myoutput.gender,
        });
    }

    render() {
        const { trained, net, output } = this.state

        if (trained === false) {
            return (
                <div>
                    Hold on...
                </div>
            );
        }

        if (trained && output != null ) {
        
            var gender = output > .5 ? "boy" : "girl";
            console.log(gender);

            return (
                <div className="Result-Container">
                    <table>
                        <thead>
                            <th>Gender</th>
                            <th>Likelihood</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{gender}</td>
                                <td>{output}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            )
        }
    }
}

export default Brain;